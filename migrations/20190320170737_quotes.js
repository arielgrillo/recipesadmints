
exports.up = function(knex, Promise) {
    return knex.schema.createTable('quotes', table => {
        table.increments('id')
        table.string('name')
        table.string('description')
        table.integer('clientId')
        table.decimal('totalCostPrice')
        table.decimal('totalSellPrice')
        table.decimal('partialPayment')
        table.integer('state') //maps to enum
        table.timestamps() //It adds two columns created_at and updated_at
        table.foreign('clientId').references('clients.id')
    });
  
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('quotes')
  
};
