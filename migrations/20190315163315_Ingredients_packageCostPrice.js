
exports.up = function(knex, Promise) {
    return knex.schema.table('ingredients', (t) => {
        t.decimal('packageCostPrice').notNull().defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('ingredients', (t) => {
        t.dropColumn('packageCostPrice');
    });
};
