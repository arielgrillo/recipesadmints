
exports.up = function(knex, Promise) {
    return knex.schema.table('ingredients', (t) => {
        t.decimal('packageQuantity').notNull().defaultTo(0);
        t.decimal('costPrice').notNull().defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('ingredients', (t) => {
        t.dropColumn('packageQuantity');
        t.dropColumn('costPrice');
    });
  
};
