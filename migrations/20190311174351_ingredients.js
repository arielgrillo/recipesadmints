
exports.up = function(knex, Promise) {
    return knex.schema.createTable('ingredients', table => {
        table.increments('id')
        table.string('name')
        table.integer('unitId')
        table.timestamps() //It adds two columns created_at and updated_at
        table.foreign('unitId').references('units.id')
    });
  
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('ingredients')
};
