
exports.up = function(knex, Promise) {
    //alter table Ingredients drop constraint [DF__ingredien__costP__45F365D3]
    //alter table Ingredients drop column [costPrice]
    return knex.schema.table('ingredients', (t) => {
        t.dropColumn('costPrice');
    });
};

exports.down = function(knex, Promise) {
  
};
