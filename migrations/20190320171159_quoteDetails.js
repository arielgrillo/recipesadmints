
exports.up = function(knex, Promise) {
    return knex.schema.createTable('quoteDetails', table => {
        table.increments('id')
        table.string('details')
        table.integer('quoteId')
        table.integer('recipeId')
        table.decimal('quantity')
        table.decimal('costPrice')
        table.timestamps() //It adds two columns created_at and updated_at
        table.foreign('recipeId').references('recipes.id')
        table.foreign('quoteId').references('quotes.id')

    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('quoteDetails')
};
