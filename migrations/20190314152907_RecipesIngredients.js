
exports.up = function(knex, Promise) {
    return knex.schema.createTable('recipes_ingredients', table => {
        table.increments('id')
        table.integer('RecipeId')
        table.integer('IngredientId')
        table.decimal('quantity')
        table.timestamps()
        table.foreign('RecipeId').references('recipes.id')
        table.foreign('IngredientId').references('ingredients.id')
    });
 
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('recipes_ingredients')
};
