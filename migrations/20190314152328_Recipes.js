
exports.up = function(knex, Promise) {
    return knex.schema.createTable('recipes', table => {
        table.increments('id')
        table.string('name')
        table.text('details')
        table.timestamps()
    });
  
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('recipes')
};
