
exports.up = function(knex, Promise) {
    return knex.schema.table('recipes', (t) => {
        t.decimal('costPrice').notNull().defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('recipes', (t) => {
        t.dropColumn('costPrice');
    });

};
