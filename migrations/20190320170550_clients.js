
exports.up = function(knex, Promise) {
    return knex.schema.createTable('clients', table => {
        table.increments('id')
        table.string('fullName')
        table.string('phone')
        table.string('mail')
        table.timestamps() //It adds two columns created_at and updated_at
    });
  
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('clients')
};
