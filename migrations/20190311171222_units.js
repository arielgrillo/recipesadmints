
exports.up = function(knex, Promise) {
    return knex.schema.createTable('units', table => {
        table.increments('id')
        table.string('name')
        table.string('symbol')
        table.timestamps() //It adds two columns created_at and updated_at
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('units')
  
};
