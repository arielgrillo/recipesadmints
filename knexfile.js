// Update with your config settings.
module.exports = {
  development: {
    client: 'mssql',
    connection: {
      user: 'sa',
      password:'sa',
      //server: 'ATENAS\\SQLEXPRESS',
      server: "localhost",
      database:"recipeAdminTs",
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
};
