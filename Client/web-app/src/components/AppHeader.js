import React from 'react';
import { Link } from 'react-router-dom';
import {AppBar, Button,Toolbar,Typography,withStyles} from '@material-ui/core'

import LoginButton from './LoginButton';

const styles = {
  flex: {
    flex: 1,
  },
};


const AppHeader = ({classes}) => (
    <AppBar position="static">
        <Toolbar>
            <Typography variant="h6" color="inherit">Recetas y algo más</Typography>
            <div className={classes.flex} />
            <Button color="inherit" component={Link} to="/">Inicio</Button>
            <Button color="inherit" component={Link} to="/recipe">Recetas</Button>
            <Button color="inherit" component={Link} to="/ingredient">Ingredientes</Button>
            <Button color="inherit" component={Link} to="/unit">Unidades</Button>
            <Button color="inherit" component={Link} to="/quote">Presupuestos</Button>
            <Button color="inherit" component={Link} to="/client">Clientes</Button>            
            <div className={classes.flex} />
            <LoginButton />            
        </Toolbar>
    </AppBar>
);

export default withStyles(styles)(AppHeader);