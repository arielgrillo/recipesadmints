import React, {Component, Fragment} from 'react';
import {withAuth} from '@okta/okta-react';
import {withRouter, Route, Redirect, Link} from 'react-router-dom';
import {withStyles, Typography, IconButton, Paper, List,ListItem, ListItemText, ListItemSecondaryAction, Fab} from '@material-ui/core';
import {Delete as DeleteIcon, Add as AddIcon} from '@material-ui/icons';
import moment from 'moment';
import {find, orderBy} from 'lodash';
import {compose} from 'recompose';
import ClientEditor from '../components/ClientEditor';

const styles = theme => ({
    clients: {
        marginTop: 2 * theme.spacing.unit
    },
    fab: {
        position: 'absolute',
        bottom: 3 * theme.spacing.unit,
        right:  3 * theme.spacing.unit,
        [theme.breakpoints.down('xs')]: {
            bottom: 2 * theme.spacing.unit,
            right: 2 * theme.spacing.unit,
        },
    },
});

const API = process.env.REACT_APP_API

class ClientsManager extends Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            clients: [],
        }
    }
    
    componentDidMount(){
        this.getClients();
    }

    async fetch(method, endpoint, body){
        try{
            const response = await fetch(`${API}${endpoint}`,{
                method,
                body: body && JSON.stringify(body),
                headers:{
                    'content-type':'application/json',
                    accept:'application/json',
                    authorization: `Bearer ${await this.props.auth.getAccessToken()}`
                },
            });
            return await response.json();
        }catch(error){
            console.log(error);
        }
    }

    async getClients(){
        this.setState({
            loading: false,
            clients: await this.fetch('get','/client')
        });
    }

    saveClient = async (client) => {
        if(client.id){
            var id = client.id
            delete client.id
            await this.fetch('put',`/client/${id}`,client)
        } else {
            await this.fetch('post','/client',client);
        }

        this.props.history.goBack();
        this.getClients();
    }

    async deleteClient(client) {
        if(window.confirm(`Are you sure you want to delete "${client.title}"`))
        await this.fetch('delete',`/client/${client.id}`);
        this.getClients();
    }

    renderClientEditor = ({ match: { params: { id } } }) => {
        if (this.state.loading) return null;
        const client = find(this.state.clients, { id: Number(id) });
    
        if (!client && id !== 'new') return <Redirect to="/client" />;
    
        return <ClientEditor client={client} onSave={this.saveClient} />;
    };

    render() {
        const { classes } = this.props;
        return (
            <Fragment>
            <Typography variant="h6">Clientes</Typography>
            {this.state.clients.length > 0 ? (
                <Paper elevation={1} className={classes.clients}>
                <List>
                    {orderBy(this.state.clients, ['id'], ['asc']).map(client => (
                    <ListItem key={client.id} button component={Link} to={`/client/${client.id}`}>
                        <ListItemText
                        primary={client.fullName}
                        secondary={client.phone + ' - ' + client.mail + ' - ' + (client.updated_at && `Actualizado ${moment(client.updated_at).fromNow()}`)}
                        />
                        <ListItemSecondaryAction>
                        <IconButton onClick={() => this.deleteClient(client)} color="inherit">
                            <DeleteIcon />
                        </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    ))}
                </List>
                </Paper>
            ) : (
                !this.state.loading && <Typography variant="subheading">No clients to display</Typography>
            )}
            <Fab
                color="secondary"
                aria-label="add"
                className={classes.fab}
                component={Link}
                to="/client/new"
            >
                <AddIcon />
            </Fab>
            <Route exact path="/client/:id" render={this.renderClientEditor} />
            </Fragment>
        );
        
    }
}

export default compose(
    withAuth,
    withRouter,
    withStyles(styles),
  )(ClientsManager);
