import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import QuoteList from './quote/QuoteList';
import Header from './Header';
import history from '../history';
import QuoteCreate from './quote/QuoteCreate';
import QuoteEdit from './quote/QuoteEdit';

class App extends React.Component{
    render(){
        return(
            <div className="ui container">
                <Router history={history}>
                    <div>
                        <Header />
                        <Route path="/quote" exact component={QuoteList} />
                        <Route path="/quote/new" exact component={QuoteCreate} />
                        <Route path="/quote/edit/:id" exact component={QuoteEdit} />
                    </div>
                </Router>
            </div>
        );
    }    
}

export default App;