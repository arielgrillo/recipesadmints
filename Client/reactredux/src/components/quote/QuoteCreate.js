import React from 'react';
import { connect } from 'react-redux';
import { createQuote} from '../../actions/quoteActions';
import { fetchClients } from '../../actions/clientActions'
import QuoteForm from './QuoteForm';

class QuoteCreate extends React.Component{
    
    componentDidMount(){
        this.props.fetchClients();
    }

    onSubmit = (formValues) => {
        this.props.createQuote(formValues);
    }
    render(){        
        return(
            <div>
                <h3>Creación de presupuesto</h3>
                <QuoteForm onSubmit={this.onSubmit} allClients={this.props.allClients}></QuoteForm>
            </div>
        );
    }
}

const mapStateToProps = (state)=>{
    return{
        allClients: Object.values(state.clients)
    }
}
export default connect(mapStateToProps,{createQuote, fetchClients})(QuoteCreate);