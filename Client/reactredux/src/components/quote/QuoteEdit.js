import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {fetchQuote, editQuote} from '../../actions/quoteActions';
import QuoteForm from './QuoteForm';

class QuoteEdit extends React.Component{

    onSubmit = (formValues) => {
        this.props.editQuote(this.props.match.params.id, formValues);
    }

    render(){
        if(!this.props.quote){
            return <div>Loading...</div>
        }
        
        return(
            <div>
                <h3>Edición de presupuesto</h3>
                <QuoteForm
                    clientDetails={this.props.clients[this.props.quote.clientId]}
                    initialValues={_.pick(this.props.quote,'name','description','clientId','totalCostPrice','totalSellPrice','partialPayment','state')}
                    onSubmit={this.onSubmit}>
                </QuoteForm>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    
    return {
        quote: state.quotes[ownProps.match.params.id],
        clients: state.clients
    };
}

export default connect(mapStateToProps,{fetchQuote,editQuote})(QuoteEdit);