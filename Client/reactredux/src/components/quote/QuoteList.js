import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {fetchQuotes} from '../../actions/quoteActions';

class QuoteList extends React.Component{

    componentDidMount() {
        this.props.fetchQuotes();
    }

    renderButtons(quote){
        if(quote.userId === this.props.currentUserId){
            return(
                <div className="right floated content">
                    <Link
                        to={`/quote/edit/${quote.id}`}
                        className="ui button primary">Editar
                    </Link>
                    {/* <Link
                        to={`/quote/delete/${quote.id}`}
                        className="ui button negative">Eliminar
                    </Link> */}
                </div>
            );
        }
    }
    renderCreate(){
        //if(this.props.isSignedIn){
            return(
                <div style={{textAlign:'right'}}>
                    <Link
                        to={'/quote/new'}
                        className="ui button primary">Nuevo presupuesto
                    </Link>
                </div>
            );
        //}
    }

    renderClient(clientId){
        if(this.props.clients[clientId]){
            const {fullName, phone} = this.props.clients[clientId];
            return(
                <div className="description">{ `${fullName} - ${phone}` }</div>
            );
        }
    }

    renderList(){
        return this.props.quotes.map(quote =>{
                return (
                    <div className="item" key={quote.id}>
                        {this.renderButtons(quote)}
                        <i className="large middle aligned icon file image outline" />
                        <div className="content">
                            <Link 
                                to={`/quote/${quote.id}`}
                                className="header">
                                {quote.name}
                            </Link>
                            <div className="description">{quote.description}</div>
                            {this.renderClient(quote.clientId)}
                        </div>
                    </div>
                );
            }
        )
    }

    render(){
        return(
            <div>
                <h2>Presupuestos pendientes</h2>
                <div className="ui celled list">
                    {this.renderList()}
                </div>
                {this.renderCreate()}
            </div>
        );
    }
}

const mapStateToProps = (state)=>{
    
    return{
        quotes: Object.values(state.quotes),
        clients: state.clients
    }
}

export default connect(mapStateToProps,{fetchQuotes})(QuoteList);