import React from 'react';
import { Field, reduxForm} from 'redux-form';

class QuoteForm extends React.Component{

    renderError ({error, touched}){
        if(touched && error){
            return(
                <div className="ui error message">
                    <div className="header">
                        {error}
                    </div>
                </div>
            );
        }
    }

    renderInput = ({input, label, meta}) => {
        
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
        return(
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete="off" />
                {this.renderError(meta)}
            </div>
            
        )
    }

    renderClientList = () => {
        console.log(this.props.allClients);
        return this.props.allClients.map(client => {
            return (
                <option value={client.id} key={client.id}>{client.fullName}</option>
            );
        })
    }
    renderClientDropDown = ({input, label, meta}) => {
        return (
            <div className='field'>
                <label>{label}</label>
                <select className='ui dropdown' {...input}>
                    {this.renderClientList()}
                </select>
                {this.renderError(meta)}
            </div>
        );
    }

    renderClient = ({input, label, meta}) => {
        if(!this.props.initialValues){
            if(this.props.allClients)
            {
                return this.renderClientDropDown({input, label, meta});
            }
            else
            {
                return <div>Error al cargar clientes.</div>
            }
        }else{
            return (<div className="field">
                <label>{label}</label>
                <label>{this.props.clientDetails.fullName}</label>
            </div>);
        }
    }

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    }

    render(){
        const isEditMode = !this.props.initialValues ? 'Modo creación': 'Modo edición'
        return(
            <div>
                <h3>{isEditMode}</h3>
                <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                    <Field name="name" component={this.renderInput} label="Nombre presupuesto"></Field>
                    <Field name="description" component={this.renderInput} label="Descripción"></Field>
                    <Field name="clientId" component={this.renderClient} label="Cliente"></Field>
                    <Field name="totalCostPrice" component={this.renderInput} label="Precio de costo"></Field>
                    <Field name="totalSellPrice" component={this.renderInput} label="Precio de venta"></Field>
                    <Field name="partialPayment" component={this.renderInput} label="Pago parcial"></Field>
                    <Field name="state" component={this.renderInput} label="Estado"></Field>
                    <button className="ui button primary">Guardar</button>
                </form>
            </div>
        );
    }
}

const validate = (formValues) => {
    const errors = {}

    if(!formValues.name){
        errors.name = 'Se debe ingresar un nombre para el presupuesto'
    }

    if(!formValues.clientId){
        errors.clientId = 'Se debe ingresar un cliente para el presupuesto'
    }

    if(!formValues.totalCostPrice){
        errors.totalCostPrice = 'Se debe ingresar un el costo del presupuesto'
    }

    if(!formValues.state){
        errors.state = 'Se debe ingresar el estado del presupuesto'
    }
    return errors;
}

export default reduxForm({
    form: 'quoteForm',
    validate: validate
})(QuoteForm);

