import React from 'react';
import { Link } from 'react-router-dom';
//import GoogleAuth from './GoogleAuth';

class Header extends React.Component{
    render(){
        return(
            <div className="ui secondary pointing menu">
                <Link to="/" className="item">Recipes Admin</Link>
                <div className="right menu">
                    <Link to="/quote" className="item">Presupuestos</Link>
                    <Link to="/client" className="item">Clientes</Link>
                    <Link to="/recipes" className="item">Recetas</Link>
                </div>
            </div>
        );
    }
}

export default Header;