import {
    FETCH_QUOTE,
    FETCH_QUOTES,
    EDIT_QUOTE,
    DELETE_QUOTE,
    CREATE_QUOTE
} from '../actions/quoteTypes';
import _ from 'lodash';

export default (state={}, action) => {
    switch(action.type){
        case FETCH_QUOTES:
            return {...state, ..._.mapKeys(action.payload,'id')};
        case FETCH_QUOTE:
            return {...state, [action.payload.id]: action.payload};
        case CREATE_QUOTE:
            return {...state, [action.payload.id]: action.payload};
        case EDIT_QUOTE:
            return {...state, [action.payload.id]: action.payload};
        case DELETE_QUOTE:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}