import {
    FETCH_CLIENT,
    FETCH_CLIENTS,
    EDIT_CLIENT,
    DELETE_CLIENT,
    CREATE_CLIENT
} from '../actions/clientTypes';
import _ from 'lodash';

export default (state={}, action) => {
    switch(action.type){
        case FETCH_CLIENTS:
            return {...state, ..._.mapKeys(action.payload,'id')};
        case FETCH_CLIENT:
            return {...state, [action.payload.id]: action.payload};
        case CREATE_CLIENT:
            return {...state, [action.payload.id]: action.payload};
        case EDIT_CLIENT:
            return {...state, [action.payload.id]: action.payload};
        case DELETE_CLIENT:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}