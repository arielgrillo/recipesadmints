import { combineReducers } from 'redux';
import { reducer as formReducer} from 'redux-form';
import quoteReducer from './quoteReducer';
import clientReducer from './clientReducer';

export default combineReducers(
    {
        form: formReducer,
        quotes: quoteReducer,
        clients: clientReducer
    }
)