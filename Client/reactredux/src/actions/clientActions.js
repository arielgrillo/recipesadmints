import ApiAccess from '../api/ApiAccess';
import history from '../history';
import {
    CREATE_CLIENT,
    EDIT_CLIENT,
    DELETE_CLIENT,
    FETCH_CLIENT,
    FETCH_CLIENTS
} from './clientTypes';

export const createClient = formValues => async (dispatch, getState) => {
    const response = await ApiAccess.post('/client',{...formValues});
    dispatch({type:CREATE_CLIENT,payload:response.data});

    history.push('/');
}

export const fetchClients = () => async (dispatch) => {
    const response = await ApiAccess.get('/client');
    dispatch({type:FETCH_CLIENTS, payload:response.data});
}

export const fecthClient = (id) => async (dispatch) => {
    const response = await ApiAccess.get(`/client/${id}`);
    dispatch({type:FETCH_CLIENT, payload:response.data[0]})
}

export const editClient = (id, formValues) => async (dispatch) => {
    const response = await ApiAccess.patch(`/client/${id}`,{...formValues});
    dispatch({type:EDIT_CLIENT, payload:response.data});
    history.push('/');
}

export const deleteClient = (id) => async (dispatch) => {
    await ApiAccess.delete(`/client/${id}`);
    dispatch({type:DELETE_CLIENT, payload:id});
    history.push('/');
}