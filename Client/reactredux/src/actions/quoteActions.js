import _ from 'lodash';
import ApiAccess from '../api/ApiAccess';
import history from '../history';
import {
    CREATE_QUOTE,
    EDIT_QUOTE,
    DELETE_QUOTE,
    FETCH_QUOTE,
    FETCH_QUOTES
} from './quoteTypes';
import { fecthClient, fetchClients } from './clientActions';


export const createQuote = formValues => async (dispatch, getState) => {
    const response = await ApiAccess.post('/quote',{...formValues});
    dispatch({type:CREATE_QUOTE,payload:response.data});
    history.push('/quote');
}

export const fetchQuotes = () => async (dispatch, getState) => {
    const response = await ApiAccess.get('/quote');
    dispatch({type:FETCH_QUOTES, payload:response.data});

    _.chain(getState().quotes)
    .map('clientId')
    .uniq()
    .forEach(id => dispatch(fecthClient(id)))
    .value();
}

export const fetchQuote = (id) => async (dispatch) => {
    const response = await ApiAccess.get(`/quote/${id}`);
    dispatch({type:FETCH_QUOTE, payload:response.data})
}

export const editQuote = (id, formValues) => async (dispatch) => {
    const response = await ApiAccess.put(`/quote/${id}`,{...formValues});
    dispatch({type:EDIT_QUOTE, payload:response.data});
    history.push('/quote');
}

export const deleteQuote = (id) => async (dispatch) => {
    await ApiAccess.delete(`/quote/${id}`);
    dispatch({type:DELETE_QUOTE, payload:id});
    history.push('/');
}