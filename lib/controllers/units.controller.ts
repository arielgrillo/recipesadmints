import BaseController from "./base.controller";

export default class UnitController extends BaseController{
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }
}