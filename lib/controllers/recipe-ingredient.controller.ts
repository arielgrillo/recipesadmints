import { Request, Response} from "express";
import BaseController from "./base.controller";
import BaseModel from "../models/baseModel";
import RecipeIngredientService from "../services/recipe-ingredient.service";


export default class RecipeIngredientController extends BaseController {
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }

    public getByRecipeId = (req: Request, res: Response) => {
        let service: RecipeIngredientService = new RecipeIngredientService(this.tableName,this.primaryKeyName);

        service.getByRecipeId(req.params.elementId).then((units:BaseModel[]) => {
            if(units.length > 0){
                res.send(units);
            }else{
                res.send(`Items with ${this.primaryKeyName}: ${req.params.elementId} does not exist.`);
            }
        }).catch((err) => {res.send(err)})
    }
    public getByIngredientId = (req: Request, res: Response) => {
        let service: RecipeIngredientService = new RecipeIngredientService(this.tableName,this.primaryKeyName);

        service.getByIngredientId(req.params.elementId).then((units:BaseModel[]) => {
            if(units.length > 0){
                res.send(units);
            }else{
                res.send(`Items with ${this.primaryKeyName}: ${req.params.elementId} does not exist.`);
            }
        }).catch((err) => {res.send(err)})
    }

}
