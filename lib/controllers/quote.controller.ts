import { Request, Response} from "express";
import BaseController from "./base.controller";
import QuoteDetailsModel from "../models/quoteDetail.model";
import QuoteService from "../services/quote.service";
import baseModel from "../models/baseModel";
import QuoteModel from "../models/quote.model";
import QuoteCostPriceModel from "../models/quoteCostPrice";


export default class QuoteController extends BaseController {

    getDetailsByQuoteId = (req: Request, res: Response): void => {
        let service = new QuoteService("Quotes","Id");
        service.getDetailsByQuoteId(req.params.elementId).then((result: QuoteDetailsModel[]) => {
            if(result.length > 0){
                res.send(result);
            }else
            {
                res.send(`Elements with quote Id: ${req.params.elementId} do not exist.`);
            }
        }).catch((err:any) => {
            res.send(err);
        });


    }

    deleteDetailsByQuoteId = (req: Request, res: Response): void => {
        let service = new QuoteService("Quotes","Id");
        service.deleteDetailsByQuoteId(req.params.elementId).then((result: boolean) => {
            if(result){
                res.send(`deleted items with quote id: ${this.primaryKeyName}: ${req.params.elementId}`);
            }else
            {
                res.send(`Elements with quote Id: ${req.params.elementId} could not be deleted.`);
            }
        }).catch((err:any) => {
            res.send(err);
        });

    }

    calculateCostPrice = (req:Request,res:Response) => {
        let quoteServ = new QuoteService("Quotes","Id");
        quoteServ.getById(req.params.elementId).then((quote:baseModel[]) => {
            return quote[0] as QuoteModel;
        }).then((quote: QuoteModel) => {
            quoteServ.calculateCostPrice(quote).then(
                (quoteCostPrice:QuoteCostPriceModel|string) => {
                    res.send(quoteCostPrice);
                }
            ).catch(
                (err) => res.send(err))
        })
    }

    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }
}
