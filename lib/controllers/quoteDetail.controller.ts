import BaseController from "./base.controller";

export default class QuoteDetailController extends BaseController {
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }
}
