import { Request, Response} from "express";
import BaseController from "./base.controller";
import RecipeService from "../services/recipe.service";
import baseModel from "../models/baseModel";
import RecipeModel from "../models/recipe.model";
import RecipeCostPriceModel from "../models/recipeCostPrice.model";

export default class RecipeController extends BaseController {
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }

    updateCostPrice = (req:Request, res:Response) => {
        let recServ = new RecipeService("Recipes","Id");
        recServ.getById(req.params.elementId).then((rec:baseModel[]) => {
            return rec[0] as RecipeModel;
        }).then((recipe: RecipeModel) => {
            recServ.updateCostPrice(recipe).then(
                (result: Boolean) => {
                    if(result){
                        res.send("cost price was updated");
                    }
                }
            ).catch(
                (err) => res.send(err))
        })
    }

    calculateCostPrice = (req:Request,res:Response) => {
        let recServ = new RecipeService("Recipes","Id");
        recServ.getById(req.params.elementId).then((rec:baseModel[]) => {
            return rec[0] as RecipeModel;
        }).then((recipe: RecipeModel) => {
            recServ.calculateCostPrice(recipe).then(
                (recipeCostPrice:RecipeCostPriceModel|string) => {
                    res.send(recipeCostPrice);
                }
            ).catch(
                (err) => res.send(err))
        })
    }
}
