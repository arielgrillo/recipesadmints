import BaseController from "./base.controller";

export default class ClientController extends BaseController {
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }
}
