import { Request, Response} from "express";
import BaseService from "../services/base.service"
import BaseModel from "../models/baseModel";
import baseModel from "../models/baseModel";

export default abstract class BaseController{
    public tableName: string;
    public primaryKeyName: string;

    public service: BaseService; 
    
    constructor(tableName:string, pkName:string){
        this.tableName = tableName;
        this.primaryKeyName = pkName;
        this.service = new BaseService(this.tableName,this.primaryKeyName);
    }
    
    public addNew = (req: Request, res: Response) => {
        let model: baseModel = req.body;
        model.created_at = new Date();

        this.service.addNew(model).then((id:number)=>{
            res.send({
                id: id,
                data: model
            });
        }).catch((err) => {res.send(err)})
    }
    public getAll = (req: Request, res: Response) => {
        this.service.getAll().then((models:BaseModel[]) => {
            res.send(models);
       }).catch((err) => {res.send(err)});
   }
    public getByID = (req: Request, res: Response) => {
        this.service.getById(req.params.elementId).then((models:BaseModel[]) => {
            if(models.length > 0){
                res.send(models);
            }else{
                res.send(`Items with ${this.primaryKeyName}: ${req.params.elementId} does not exist.`);
            }
        }).catch((err) => {res.send(err)})
    }
    public update = (req: Request, res: Response) => {
        let model: baseModel = req.body;
        model.updated_at = new Date();

        this.service.update(model, req.params.elementId).then((result: boolean) => {
            if(result){
                //res.send(`Updated item with id: ${this.primaryKeyName}: ${req.params.elementId}`);
                res.send({id: req.params.elementId});
            }
            else{
                res.send(`Element with Id: ${req.params.elementId} does not exist in table: ${this.tableName}`);
            }
        }).catch((err:any) => {
            res.send(err);
        });

    }

    public delete = (req: Request, res: Response): void => {
        this.service.delete(req.params.elementId).then((result: boolean) => {
            if(result){
                //res.send(`deleted item with id: ${this.primaryKeyName}: ${req.params.elementId}`);
                res.send({id: req.params.elementId});
            }else
            {
                res.send(`Element with Id: ${req.params.elementId} does not exist in table: ${this.tableName}`);
            }
        }).catch((err:any) => {
            res.send(err);
        });
    }

}