import BaseController from "./base.controller";

export default class IngredientController extends BaseController {
    constructor(tableName: string, pkName:string){
        super(tableName, pkName);
    }
}
