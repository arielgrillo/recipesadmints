import knex from "../dataAccess/dbConnection";
import RecipeIngredientModel from "../models/recipe-ingredient.model";
import RecipeIngredientInterface from "./interfaces/recipe-ingredient.interface";
import BaseService from "./base.service";

export default class RecipeIngredientService extends BaseService implements RecipeIngredientInterface<RecipeIngredientModel>{
   
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};

    public async getByRecipeId(recipeId: number): Promise<RecipeIngredientModel[]> {
        return await knex(this.tableName).select().where("recipeId",recipeId);
    }

    public async getByIngredientId(ingredientId: number): Promise<RecipeIngredientModel[]> {
        return await knex(this.tableName).select().where("ingredientId",ingredientId);
    }

}