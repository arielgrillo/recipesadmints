import IngredientModel from "../models/ingredient.model";
import IngredientInterface from "./interfaces/ingredient.interface";
import BaseService from "./base.service";

export default class IngredientService extends BaseService implements IngredientInterface<IngredientModel>{
    
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};
}