import QuoteDetailModel from "../models/QuoteDetail.model";
import QuoteDetailInterface from "./interfaces/quoteDetail.interface";
import BaseService from "./base.service";

export default class QuoteDetailService extends BaseService implements QuoteDetailInterface<QuoteDetailModel>{
    
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};
}