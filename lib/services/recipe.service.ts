import RecipeModel from "../models/recipe.model";
import RecipeInterface from "./interfaces/recipe.interface";
import BaseService from "./base.service";
import RecipeIngredientService from "./recipe-ingredient.service";
import IngredientService from "./ingredient.service";
import IngredientModel from "../models/ingredient.model";
import RecipeIngredintModel from "../models/recipe-ingredient.model";
import RecipeCostPriceModel from "../models/recipeCostPrice.model";
import { reject } from "bluebird";

export default class RecipeService extends BaseService implements RecipeInterface<RecipeModel>{
    
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};

    public updateCostPrice(model: RecipeModel): Promise<Boolean>{
        let result = new Promise<Boolean>((resolve, reject) => {
            this.calculateCostPrice(model).then(
            (recipeCostPrice: RecipeCostPriceModel) => {
                let recipeId = recipeCostPrice.Recipe.id;
                let recipe = new RecipeModel(recipeCostPrice.Recipe.name,recipeCostPrice.Recipe.details,recipeCostPrice.Recipe.costPrice);
                recipe.created_at = recipeCostPrice.Recipe.created_at
                this.update(recipe,recipeId).then((result) => {
                    resolve(result)
                }).catch((err) => {reject(err)});
                
            }).catch((err) => {reject(err)})
        });
        return result;
    }

    public calculateCostPrice (model: RecipeModel): Promise<RecipeCostPriceModel> {
        let ret = new Promise<RecipeCostPriceModel>((resolve,reject)=>{
            let recIngServ = new RecipeIngredientService("recipes_ingredients", "id");
            let ingServ = new IngredientService("Ingredients","Id");
            
            recIngServ.getByRecipeId(model.id).then(
                (recsIngs: RecipeIngredintModel[]) => {
                    let aux: Promise<any>[] = [];
                    recsIngs.forEach(
                        (recIng: RecipeIngredintModel)=> {
                            aux.push(ingServ.getById(recIng.IngredientId));
                        }
                    );
                    let costPrice: number = 0;
                    Promise.all(aux).then(
                        (values: IngredientModel[][]) => {
                            let recipeCostPrice: RecipeCostPriceModel = new RecipeCostPriceModel();
                            values.forEach((ingredient: IngredientModel[])=> {
                                let recIng = recsIngs.filter(v=>v.IngredientId===ingredient[0].id)[0];
                                ingredient[0].costPrice = (recIng.quantity * ingredient[0].packageCostPrice)/ingredient[0].packageQuantity;
                                recipeCostPrice.Ingredients.push(ingredient[0]);
                                costPrice+=ingredient[0].costPrice
                            })
                            model.costPrice = costPrice;
                            recipeCostPrice.Recipe = model;
                             
                            resolve(recipeCostPrice);
                        }
                    ).catch((err)=>reject(err));
                }
            ).catch((err)=>reject(err));
        });
        return ret;
    }
}
