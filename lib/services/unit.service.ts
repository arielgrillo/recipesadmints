import knex from "../dataAccess/dbConnection";
import UnitModel from "../models/unit.model";
import UnitInterface from "./interfaces/unit.interface";
import BaseService from "./base.service";

export default class UnitService extends BaseService implements UnitInterface<UnitModel>{
    
    constructor(tableName: string, primaryKeyName: string){super(tableName, primaryKeyName)};


}