import QuoteModel from "../models/quote.model";
import QuoteInterface from "./interfaces/quote.interface";
import BaseService from "./base.service";
import QuoteDetailsModel from "../models/quoteDetail.model";
import knex from "../dataAccess/dbConnection";
import QuoteCostPriceModel from "../models/quoteCostPrice";
import RecipeService from "./recipe.service";
import RecipeModel from "../models/recipe.model";
import QuoteDetailService from "./quoteDetail.service";

export default class QuoteService extends BaseService implements QuoteInterface<QuoteModel>{
   
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};

    public async getDetailsByQuoteId(quoteId: number): Promise<QuoteDetailsModel[]> {
        return await knex("QuoteDetails").select().where("quoteId",quoteId);
    }
    public async deleteDetailsByQuoteId(quoteId: number): Promise<boolean> {
        return await this.getDetailsByQuoteId(quoteId).then((model: QuoteDetailsModel[]) => {
            if(model.length > 0){
                knex("QuoteDetails").where("quoteId","=",quoteId)
                .del()
                .catch((err:any) => {
                    throw (err);
                })
                return true;
            }
            else{
                return false
            }
        });
    }

    public calculateCostPrice (model: QuoteModel): Promise<QuoteCostPriceModel|string> {
        let ret = new Promise<QuoteCostPriceModel>((resolve,reject)=>{
            let quoteServ = new QuoteService("Quotes", "id");
            let recipeServ = new RecipeService("recipes","Id");
            
            quoteServ.getDetailsByQuoteId(model.id).then(
                (details: QuoteDetailsModel[]) => {
                    let aux: Promise<any>[] = [];
                    details.forEach(
                        (detail: QuoteDetailsModel)=> {
                            aux.push(recipeServ.getById(detail.recipeId));
                        }
                    );
                    let costPrice: number = 0;
                    Promise.all(aux).then(
                        (values: RecipeModel[][]) => {
                            let quoteCostPrice: QuoteCostPriceModel = new QuoteCostPriceModel();
                            let quoteDEtailSrv = new QuoteDetailService("quoteDetails","id");
                            values.forEach((Recipes: RecipeModel[])=> {
                                let detail = details.filter(v=>v.recipeId===Recipes[0].id)[0];
                                detail.costPrice = Recipes[0].costPrice*detail.quantity;
                                costPrice+=detail.costPrice;
                                quoteCostPrice.Recipes.push(Recipes[0]);
                                quoteCostPrice.QuoteDetails = details;

                                let qd = new QuoteDetailsModel();
                                qd.costPrice = detail.costPrice;
                                qd.created_at = detail.created_at;
                                qd.updated_at = new Date();
                                qd.details = detail.details;
                                qd.quantity = detail.quantity;
                                qd.quoteId = detail.quoteId;
                                qd.recipeId = detail.recipeId;
                                quoteDEtailSrv.update(qd, detail.id);
                            })
                            model.totalCostPrice = costPrice;
                            quoteCostPrice.Quote = model;

                            let quote = new QuoteModel();
                            quote.clientId = model.clientId;
                            quote.created_at = model.created_at;
                            quote.description = model.description;
                            quote.name = model.name;
                            quote.partialPayment = model.partialPayment;
                            quote.state = model.state;
                            quote.totalCostPrice = model.totalCostPrice;
                            quote.totalSellPrice = model.totalSellPrice;
                            quote.updated_at = new Date();
                            
                            quoteServ.update(quote,model.id);
                             
                            resolve(quoteCostPrice);
                        }
                    ).catch((err)=>reject(err));
                }
            ).catch((err)=>reject(err));
        });
        return ret;

    }
    
    
}