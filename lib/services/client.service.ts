import ClientModel from "../models/client.model";
import ClientInterface from "./interfaces/client.interfase";
import BaseService from "./base.service";

export default class ClientService extends BaseService implements ClientInterface<ClientModel>{
    
    constructor(tableName: string, primaryKeyName: string){super(tableName,primaryKeyName)};
}