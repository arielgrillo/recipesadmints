import Service from "./service.interface";
import QuoteDetailModel from "../../models/quoteDetail.model";

export default interface QuoteDetailInterface<T> extends Service<QuoteDetailModel>{}
