import Service from "./service.interface";
import RecipeIngredientModel from "../../models/recipe-ingredient.model";

export default interface RecipeIngredientInterface<T> extends Service<RecipeIngredientModel>{
    getByRecipeId(recipeId:number) : Promise<RecipeIngredientModel[]>

    getByIngredientId(ingredientId:number) : Promise<RecipeIngredientModel[]>
}
