
import Service from "./service.interface";
import BaseModel from "../../models/baseModel"
import QuoteModel from "../../models/quote.model";
import QuoteDetailsModel from "../../models/quoteDetail.model";
import QuoteCostPriceModel from "../../models/quoteCostPrice";

export default interface QuoteInterface<T extends BaseModel> extends Service<T>{

    getDetailsByQuoteId(getDetailsByQuoteId: number): Promise<QuoteDetailsModel[]>;

    deleteDetailsByQuoteId(deleteDetailsByQuoteId: number): void;

    calculateCostPrice(model: QuoteModel): Promise<QuoteCostPriceModel|string>;
}
