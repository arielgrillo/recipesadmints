import baseModel from "../../models/baseModel";

export default interface BaseServiceInterface<T extends baseModel>{
    getAll():Promise<T[]>;
    addNew(model: T): Promise<number>;
    getById(id:number): Promise<T[]>;
    update(model: T, id:number): Promise<boolean>;
    delete(id:number): Promise<boolean>;
}