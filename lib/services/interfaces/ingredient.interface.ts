import Service from "./service.interface";
import IngredientModel from "../../models/ingredient.model";

export default interface IngredientInterface<T> extends Service<IngredientModel>{}
