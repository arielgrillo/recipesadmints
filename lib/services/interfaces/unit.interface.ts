import Service from "./service.interface";
import UnitModel from "../../models/unit.model";


export default interface UnitInterface<T> extends Service<UnitModel>{
    
}
