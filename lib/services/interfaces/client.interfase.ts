import Service from "./service.interface";
import ClientModel from "../../models/client.model";

export default interface IngredientInterface<T> extends Service<ClientModel>{}
