import Service from "./service.interface";
import RecipeModel from "../../models/recipe.model";
import BaseModel from "../../models/baseModel";
import RecipeCostPriceModel from "../../models/recipeCostPrice.model";

export default interface RecipeInterface<T extends BaseModel> extends Service<T>{

    calculateCostPrice(model: RecipeModel): Promise<RecipeCostPriceModel>;

    updateCostPrice(model: RecipeModel): Promise<Boolean>;

}
