import knex from "../dataAccess/dbConnection";
import BaseModel from "../models/baseModel";
import BaseServiceInterface from "./interfaces/base.interface";

export default class BaseService implements BaseServiceInterface<BaseModel>{
    public tableName!: string;
    public primaryKeyName!: string

    constructor(tableName: string, primaryKeyName: string){
        this.tableName = tableName;
        this.primaryKeyName = primaryKeyName;
    }

    public async getAll(): Promise<BaseModel[]> {
        return await knex(this.tableName).select();
    }
    public async addNew (model: BaseModel): Promise<number>{
        return await knex(this.tableName)
        .returning(this.primaryKeyName)
        .insert(model)
        .catch((err:any) => {
            throw err;
        });
    }

    public async getById (id:number): Promise<BaseModel[]> {
        return await knex(this.tableName).select().where(this.primaryKeyName,id);
    }

    public async update(model: BaseModel, id: number): Promise<boolean>{
        return await this.getById(id).then((modelInBase:BaseModel[]) => {            
            if(modelInBase.length > 0 && modelInBase[0].id > 0){
                knex(this.tableName).where(this.primaryKeyName,"=",id)
                .update(model)
                .catch((err:any) => {
                    throw err;
                });
                return true;
            }else{                
                return false;
            }
        });
    }

    public async delete(id:number):Promise<boolean>{
        return await this.getById(id).then((model: BaseModel[]) => {
            if(model.length > 0 && model[0].id > 0){
                knex(this.tableName).where(this.primaryKeyName,"=",id)
                .del()
                .catch((err:any) => {
                    throw (err);
                })
                return true;
            }
            else{
                return false
            }
        });
    }

}