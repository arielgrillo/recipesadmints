import express from "express";
import RecipeController from "../controllers/recipe.controller";

export class RecipeRoutes {
    public controller: RecipeController = new RecipeController("recipes", "Id");

    constructor(app: express.Application) {
        app.route("/recipe")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/recipe/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .delete(this.controller.delete);

        app.route("/recipe/costprice/:elementId")
        .get(this.controller.calculateCostPrice)
        .put(this.controller.updateCostPrice)

    }
}
