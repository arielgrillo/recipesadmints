import express from "express";
import RecipeIngredientController from "../controllers/recipe-ingredient.controller";

export class RecipeIngredientRoutes {
    public controller: RecipeIngredientController = new RecipeIngredientController("recipes_ingredients", "Id");

    constructor(app: express.Application) {
        app.route("/recipeIngredient")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/recipeIngredient/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .delete(this.controller.delete);

        app.route("/recipeIngredient/recipe/:elementId")
        .get(this.controller.getByRecipeId)

        app.route("/recipeIngredient/ingredient/:elementId")
        .get(this.controller.getByIngredientId)

    }
}
