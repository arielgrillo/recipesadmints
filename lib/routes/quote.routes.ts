import express from "express";
import QuoteController from "../controllers/quote.controller";

export class QuoteRoutes {
    public controller: QuoteController = new QuoteController("Quotes", "Id");

    constructor(app: express.Application) {
        app.route("/quote")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/quote/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .patch(this.controller.update)
        .delete(this.controller.delete);

        app.route("/quote/:elementId/detail/")
        .get(this.controller.getDetailsByQuoteId)
        .delete(this.controller.deleteDetailsByQuoteId);

        app.route("/quote/calculatecostprice/:elementId")
        .get(this.controller.calculateCostPrice);
        

    }
}
