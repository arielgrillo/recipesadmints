import express from "express";
import UnitController from "../controllers/units.controller";

export class UnitRoutes {
    public controller: UnitController = new UnitController("Units", "Id");

    constructor(app: express.Application) {
        app.route("/unit")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/unit/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .delete(this.controller.delete);
    }
}
