import express from "express";
import IngredientController from "../controllers/ingredients.controller";

export class IngredientRoutes {
    public controller: IngredientController = new IngredientController("Ingredients", "Id");

    constructor(app: express.Application) {
        app.route("/ingredient")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/ingredient/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .delete(this.controller.delete);
    }
}
