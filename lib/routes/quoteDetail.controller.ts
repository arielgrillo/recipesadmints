import express from "express";
import QuoteDetailController from "../controllers/quoteDetail.controller";

export class QuoteDetailRoutes {
    public controller: QuoteDetailController = new QuoteDetailController("QuoteDetails", "Id");

    constructor(app: express.Application) {
        app.route("/quotedetail")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/quotedetail/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        .delete(this.controller.delete);
    }
}
