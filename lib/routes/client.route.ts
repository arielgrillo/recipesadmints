import express from "express";
import controller from "../controllers/client.controller";

export class ClientRoutes {
    public controller: controller = new controller("clients", "Id");

    constructor(app: express.Application) {
        app.route("/client")
        .get(this.controller.getAll)
        .post(this.controller.addNew);

        app.route("/client/:elementId")
        .get(this.controller.getByID)
        .put(this.controller.update)
        //.delete(this.controller.delete); no elimina clientes
    }
}
