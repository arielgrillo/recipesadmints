import * as bodyparser from "body-parser";
import express from "express";
import {UnitRoutes} from "../lib/routes/units.routes";
import {IngredientRoutes} from "../lib/routes/ingredients.routes";
import {RecipeRoutes} from "../lib/routes/recipe.routes";
import {RecipeIngredientRoutes} from "../lib/routes/recipe-ingredient.routes";
import {ClientRoutes} from "../lib/routes/client.route";
import {QuoteRoutes} from "../lib/routes/quote.routes";
import {QuoteDetailRoutes} from "../lib/routes/quoteDetail.controller";
import cors from "cors";

class App {
    public app: express.Application;
    public unitRoutes: UnitRoutes;
    public ingredientRoutes: IngredientRoutes;
    public recipeRoutes: RecipeRoutes;
    public recipeIngredientRoutes: RecipeIngredientRoutes;
    public clientRoutes: ClientRoutes;
    public quoteRoutes: QuoteRoutes;
    public quoteDetailRoutes: QuoteDetailRoutes;

    constructor() {
        this.app = express();
        this.config();
        this.unitRoutes = new UnitRoutes(this.app);
        this.ingredientRoutes = new IngredientRoutes(this.app);
        this.recipeRoutes = new RecipeRoutes(this.app);
        this.recipeIngredientRoutes = new RecipeIngredientRoutes(this.app);
        this.clientRoutes = new ClientRoutes(this.app);
        this.quoteRoutes = new QuoteRoutes(this.app);
        this.quoteDetailRoutes = new QuoteDetailRoutes(this.app);
    }

    private config(): void {
        // this.app.options("*", cors());
        
        // this.app.use(cors());


        // support application/json type post data
        this.app.use(bodyparser.json());

        // support application/x-www-form-urlencoded post data
        this.app.use(bodyparser.urlencoded({extended: false}));

        this.app.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*"); //Origin, X-Requested-With, Content-Type, Accept, authorization 
            res.header("Access-Control-Allow-Methods", "*");
               next();
         });
    }
}

export default new App().app; 
