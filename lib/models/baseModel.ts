export default class baseModel{
    public id!: number;
    public created_at!: Date;
    public updated_at!: Date;
}