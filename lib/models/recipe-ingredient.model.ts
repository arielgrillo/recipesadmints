import BaseModel from "../models/baseModel";

export default class RecipeIngredintModel extends BaseModel{
    public RecipeID!: number;
    public IngredientId!: number;
    public quantity!: number;

    constructor(recipeID?:number,ingredientId?:number,created_at?:Date,quantity?:number){
        super();
        this.RecipeID = recipeID || -1;
        this.IngredientId = ingredientId || -1;
        this.created_at = created_at || new Date();
        this.quantity = quantity || 0;
    }
};
