import baseModel from "../models/baseModel";

export default class IngredientModel extends baseModel{
    public name!: string;
    public unitId!: number;
    public packageQuantity!: number;
    public packageCostPrice!: number;
    public costPrice!: number;


    constructor(name?:string,unitId?:number,packageQuantity?:number,costPrice?:number, packageCostPrice?:number){
        super();
        this.name = name || "";
        this.packageQuantity = packageQuantity || 0;
        this.costPrice = costPrice || 0;
        this.unitId = unitId || -1;
        this.created_at = new Date();
        this.packageCostPrice = packageCostPrice || 0;
    }
}
