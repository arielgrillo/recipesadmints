import BaseModel from "../models/baseModel";

export default class RecipeModel extends BaseModel{
    public name!: string;
    public details!: string;
    public costPrice!: number;

    constructor(name?:string,details?:string, costPrice?:number){
        super();
        this.name = name||"";
        this.details = details||"";
        this.created_at = new Date();
        this.costPrice = costPrice || 0;
    }
};
