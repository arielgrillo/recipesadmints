import BaseModel from "../models/baseModel";

export default class UnitModel extends BaseModel{
    public name!: string;
    public symbol!: string;

    constructor(name?:string,symbol?:string){
        super();
        this.name = name||"";
        this.symbol = symbol||"";
        this.created_at = new Date();
    }
};
