import baseModel from "../models/baseModel";

export default class QuoteDetailsModel extends baseModel{
    public details!: string;
    public recipeId!: number;
    public quoteId!: number;
    public quantity!: number;
    public costPrice!: number;


    constructor(name?:string,detail?:string,recipeId?:number,quoteId?:number,quantity?:number,costPrice?:number){
        super();
        this.details = detail || "";
        this.quantity = quantity || 0;
        this.costPrice = costPrice || 0;
        this.recipeId = recipeId || -1;
        this.quoteId = quoteId || -1;
        this.created_at = new Date();
    }
}
