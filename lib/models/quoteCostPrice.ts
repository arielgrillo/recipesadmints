import QuoteModel from "./quote.model"
import RecipeModel from "./recipe.model";
import QuoteDetailsModel from "./quoteDetail.model";

export default class QuoteCostPriceModel{
    public Quote!:QuoteModel;
    public QuoteDetails!: QuoteDetailsModel[];
    public Recipes!:RecipeModel[];

    constructor(){
        this.Recipes = [];
        this.QuoteDetails =[];
    }

}