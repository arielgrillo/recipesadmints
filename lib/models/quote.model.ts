import baseModel from "../models/baseModel";

export default class QuoteModel extends baseModel{
    public name!: string;
    public description!: string;
    public clientId!: number;
    public totalCostPrice!: number;
    public totalSellPrice!: number;
    public partialPayment!: number;
    public state!: number;


    constructor(name?:string,description?:string,clientId?:number,totalCostPrice?:number,totalSellPrice?:number, partialPayment?:number,state?:number){
        super();
        this.name = name || "";
        this.description = description || "";
        this.totalCostPrice = totalCostPrice || 0;
        this.totalSellPrice = totalSellPrice || 0;
        this.clientId = clientId || -1;
        this.created_at = new Date();
        this.partialPayment = partialPayment || 0;
        this.state = state || 0;
    }
}
