import IngredientModel from "./ingredient.model"
import RecipeModel from "./recipe.model";

export default class RecipeCostPriceModel{
    public Ingredients!:IngredientModel[];
    public Recipe!:RecipeModel

    constructor(){
        this.Ingredients = [];
    }

}